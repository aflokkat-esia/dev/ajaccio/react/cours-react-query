import { AddIcon, DeleteIcon, EditIcon } from "@chakra-ui/icons";
import {
  Box,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Text,
  Tooltip,
  VStack,
} from "@chakra-ui/react";
import { useQuery } from "@tanstack/react-query";
import { useState } from "react";
import { Link } from "react-router-dom";
import CreateUser from "../components/CreateUser";
import DeleteUser from "../components/DeleteUser";
import UpdateUser from "../components/UpdateUser";
import { User } from "../types/user.type";
import { getUsers } from "../utils/fetchers";

const Users = () => {
  const [mode, setMode] = useState<boolean>(true);
  const [acceptDelete, setAcceptDelete] = useState<boolean>(false);
  const [userToModify, setUserToModify] = useState<{
    id: number;
    username: string;
  }>({ id: -1, username: "Lucas" });

  const { data, isLoading } = useQuery<User[]>(["users"], getUsers, {
    staleTime: 20000,
  });

  if (isLoading) {
    return <Text>Loading...</Text>;
  }

  return (
    <VStack spacing={10} alignItems="start" p={4}>
      <Heading>Users</Heading>
      <HStack w={"100%"} spacing={20} alignItems="start">
        <VStack
          alignItems="start"
          w={"50%"}
          borderWidth={2}
          borderColor="lightgrey"
          borderRadius={20}
          p={10}
          spacing={10}
        >
          <Flex w={"100%"} justifyContent="space-between" alignItems="center">
            <Text fontSize="2xl">Liste des utilisateurs</Text>
            <IconButton
              aria-label="add"
              color="green"
              icon={<AddIcon />}
              onClick={() => {
                setMode(true);
                setAcceptDelete(false);
              }}
            />
          </Flex>
          <Box w={"100%"}>
            {data &&
              data.map((item) => (
                <Box w="100%" key={item.id}>
                  <Flex
                    w={"100%"}
                    justifyContent="space-between"
                    alignItems="center"
                  >
                    <VStack spacing={0} alignItems="start">
                      <Text>
                        <Tooltip
                          label={`Aller vers la page de ${item.username}`}
                          placement="top-end"
                        >
                          <Link
                            style={{ fontWeight: "bold" }}
                            to={`/users/${item.id}`}
                          >
                            {item.id}
                          </Link>
                        </Tooltip>{" "}
                        - {item.username}
                      </Text>
                      <Text fontSize="xs" color="gray">
                        {item.email}
                      </Text>
                    </VStack>
                    <HStack spacing={4}>
                      <IconButton
                        aria-label="edit"
                        size="sm"
                        color="orange"
                        icon={<EditIcon />}
                        onClick={() => {
                          setMode(false);
                          setUserToModify({
                            id: item.id,
                            username: item.username,
                          });
                          setAcceptDelete(false);
                        }}
                      />
                      <IconButton
                        aria-label="delete"
                        icon={<DeleteIcon />}
                        size="sm"
                        color="red"
                        onClick={() => {
                          setAcceptDelete((prev) => !prev);
                          setUserToModify({
                            id: item.id,
                            username: item.username,
                          });
                        }}
                      />
                    </HStack>
                  </Flex>
                  <Divider my={2} />
                </Box>
              ))}
          </Box>
        </VStack>
        <VStack w={"50%"}>
          {acceptDelete && (
            <DeleteUser
              id={userToModify.id}
              username={userToModify.username}
              setAcceptDelete={setAcceptDelete}
            />
          )}
          {!acceptDelete && (
            <VStack
              alignItems="start"
              w={"100%"}
              borderWidth={2}
              borderColor="lightgrey"
              borderRadius={20}
              p={10}
              spacing={10}
            >
              <>
                {mode ? (
                  <>
                    <Text fontSize="2xl">Créer un nouvel utilisateur</Text>
                    <CreateUser />
                  </>
                ) : (
                  <>
                    <Text fontSize="2xl">
                      Modifier l'utilisateur n°{userToModify.id} :{" "}
                      {userToModify.username}
                    </Text>
                    <UpdateUser
                      id={userToModify.id}
                      username={userToModify.username}
                      setMode={setMode}
                    />
                  </>
                )}
              </>
            </VStack>
          )}
        </VStack>
      </HStack>
    </VStack>
  );
};

export default Users;
