import { Box, Heading, Text } from "@chakra-ui/react";
import { useQuery } from "@tanstack/react-query";
import { useParams } from "react-router-dom";
import { User } from "../types/user.type";
import { getUserById } from "../utils/fetchers";

const UserDetails = () => {
  const { id } = useParams();

  if (!id) {
    return <Text>Problème...</Text>;
  }

  const f = new Intl.DateTimeFormat("fr-fr", {
    dateStyle: "short",
    timeStyle: "short",
  });

  const { data, isLoading } = useQuery<User>(
    [`user${id}`],
    () => getUserById(id),
    {
      staleTime: 10000,
    }
  );

  if (isLoading) {
    return <Text>Loading...</Text>;
  }

  return (
    <div>
      <Heading>UserDetails</Heading>
      {data && (
        <>
          <Text>{data.username}</Text>
          <Text>{data.email}</Text>
          {data.isAdmin && <Text>ADMIN</Text>}
          {data.messages.map((message) => (
            <Box key={message.id}>
              <Text>
                {f.format(new Date(message.createdAt))} - {message.message}
              </Text>
            </Box>
          ))}
        </>
      )}
    </div>
  );
};

export default UserDetails;
