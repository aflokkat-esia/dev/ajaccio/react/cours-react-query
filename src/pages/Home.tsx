import { Button, Heading, Input, Text, VStack } from "@chakra-ui/react";
import axios from "axios";
import { useRef } from "react";
import { useLocation } from "react-router-dom";

const Home = () => {
  const location = useLocation();

  const usernameRef = useRef<HTMLInputElement | null>(null);

  function onClickConnect() {
    axios
      .post("http://localhost:3001/auth/login", {
        username: "Nicolas",
      })
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  }

  return (
    <VStack spacing={20} alignItems="start" p={10}>
      <Heading>Home</Heading>
      {location?.state?.id && location?.state?.username && (
        <Text>
          Bienvenue, {location.state.username} n°{location.state.id}
        </Text>
      )}
      <Input placeholder="Username" ref={usernameRef} />
      <Button onClick={onClickConnect}>Connexion</Button>
    </VStack>
  );
};

export default Home;
