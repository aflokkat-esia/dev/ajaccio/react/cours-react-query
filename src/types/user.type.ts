export type CreateUser = {
  username: string;
  email?: string;
  isAdmin?: boolean;
};

export type UpdateUser = {
  id: number;
  username?: string;
  email?: string;
  isAdmin?: boolean;
};

export type User = {
  id: number;
  username: string;
  email: string;
  isAdmin: boolean;
  createdAt: string;
  messages: [
    {
      id: number;
      message: string;
      userId: number;
      createdAt: string;
    }
  ];
};
