import { CreateUser, UpdateUser } from "../types/user.type";
import { axiosInstance as axios } from "./api";

export async function getUsers() {
  const res = await axios({
    method: "GET",
    url: "/users",
  });
  return res.data;
}

export async function getUserById(id: string) {
  const res = await axios({
    method: "GET",
    url: `/users/${id}`,
  });
  return res.data;
}

export async function createUser(data: CreateUser) {
  const res = await axios({
    method: "POST",
    url: "/users",
    data: data,
  });
  return res.data;
}

export async function updateUser(data: UpdateUser) {
  const { id, ...rest } = data;
  const res = await axios({
    method: "PUT",
    url: `/users/${id}`,
    data: rest,
  });
  return res.data;
}

export async function deleteUser(id: number) {
  const res = await axios({
    method: "DELETE",
    url: `/users/${id}`,
  });
  return res.data;
}
