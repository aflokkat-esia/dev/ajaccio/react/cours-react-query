import { Button, Input } from "@chakra-ui/react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRef } from "react";
import { updateUser } from "../utils/fetchers";

const UpdateUser = ({
  id,
  username,
  setMode,
}: {
  id: number;
  username: string;
  setMode: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const updateUserMutation = useMutation(updateUser, {
    onSuccess: () => {
      queryClient.refetchQueries(["users"]);
      if (usernameRef?.current?.value) usernameRef.current.value = "";
      if (emailRef?.current?.value) emailRef.current.value = "";
      setMode(true);
    },
    onError: () => {
      console.log("Problème...");
    },
  });

  const queryClient = useQueryClient();

  const usernameRef = useRef<HTMLInputElement | null>(null);
  const emailRef = useRef<HTMLInputElement | null>(null);

  function onClickUpdateUser() {
    const truandage = { id: id };
    if (usernameRef?.current?.value)
      Object.defineProperty(truandage, "username", {
        value: usernameRef?.current?.value,
        writable: true,
        enumerable: true,
        configurable: true,
      });
    if (emailRef?.current?.value)
      Object.defineProperty(truandage, "email", {
        value: emailRef?.current?.value,
        writable: true,
        enumerable: true,
        configurable: true,
      });
    console.log(truandage);

    updateUserMutation.mutate(truandage);
  }

  return (
    <>
      <Input placeholder={username} ref={usernameRef} />
      <Input placeholder="Email" ref={emailRef} />
      <Button onClick={onClickUpdateUser}>Modifier !</Button>
    </>
  );
};

export default UpdateUser;
