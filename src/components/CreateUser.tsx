import { Button, Input } from "@chakra-ui/react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";
import { createUser } from "../utils/fetchers";

const CreateUser = () => {
  const navigate = useNavigate();
  const createUserMutation = useMutation(createUser, {
    onSuccess: ({ id, username }) => {
      queryClient.refetchQueries(["users"]);
      if (usernameRef?.current?.value) usernameRef.current.value = "";
      navigate("/", { state: { id, username } });
    },
    onError: () => {
      console.log("Problème...");
    },
  });

  const queryClient = useQueryClient();

  const usernameRef = useRef<HTMLInputElement | null>(null);

  function onClickCreateUser() {
    if (usernameRef?.current?.value) {
      createUserMutation.mutate({
        username: usernameRef.current.value,
      });
    }
  }

  return (
    <>
      <Input placeholder="Username" ref={usernameRef} />
      <Button onClick={onClickCreateUser}>Créer un nouvel utilisateur</Button>
    </>
  );
};

export default CreateUser;
