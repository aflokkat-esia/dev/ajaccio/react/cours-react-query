import { Button, HStack, Text, VStack } from "@chakra-ui/react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { deleteUser } from "../utils/fetchers";

const DeleteUser = ({
  id,
  username,
  setAcceptDelete,
}: {
  id: number;
  username: string;
  setAcceptDelete: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const deleteUserMutation = useMutation(deleteUser, {
    onSuccess: () => {
      queryClient.refetchQueries(["users"]);
      setAcceptDelete(false);
    },
    onError: () => {
      console.log("Problème...");
    },
  });

  const queryClient = useQueryClient();

  function onClickDeleteUser() {
    deleteUserMutation.mutate(id);
  }

  return (
    <VStack
      alignItems="start"
      w={"100%"}
      borderWidth={2}
      borderColor="lightgrey"
      borderRadius={20}
      p={10}
      spacing={10}
    >
      <>
        <Text fontSize="2xl">Etes-vous sûr de supprimer {username} ?</Text>
        <HStack>
          <Button colorScheme="green" onClick={onClickDeleteUser}>
            Oui
          </Button>
          <Button colorScheme="red" onClick={() => setAcceptDelete(false)}>
            Non
          </Button>
        </HStack>
      </>
    </VStack>
  );
};

export default DeleteUser;
